"""
@author: Omar Barakat
@description: This file includes the main function. It performs the following:
    1- Loads the data using DataLoader object.
    2- Extracts the features using the DataLoader object
    3- Splits data randomly into training and testing data
    4- Trains the model specified in the Parameters object
    5- Outputs the coefficients of the model and the training and testing error
"""

import random
import math 
from data_loader import DataLoader
from parameters import Parameters
from sklearn.metrics import mean_squared_error, r2_score
from sklearn import linear_model

def display_accuracy(model, data, lbls):
    # Make predictions using the data
    pred = model.predict(data)
    # The mean squared error
    print("Mean squared error: %.2f"
          % mean_squared_error(lbls, pred))
    # variance score
    print('Variance score: %.2f' % r2_score(lbls, pred))


if __name__ == '__main__':
    params = Parameters()
    loader = DataLoader(params)
    
    # load surrounding features from JSON file
    feature_dict = {}
    generator = loader.json_iterator()
    for row in generator:
        place_id, parsed_row, feature_names = loader.extract_surrounding_features(row, combine_surr_stats=params.combine_surr_stats, features_to_use=params.features_to_use)
        feature_dict[place_id] = parsed_row
    
    # load target variables from csv files
    lbl_dict = {}
    generator = loader.csv_dict_iter()
    for row in generator:
        place_id, parsed_row = loader.get_target_vars(row)
        lbl_dict[int(place_id)] = sum(parsed_row)
    
    # compute the sizes of the training and testing sets
    feature_dict_list = list(feature_dict.items())
    total_data_size = len(feature_dict_list)
    train_size = math.floor(total_data_size*params.train_test_split)
    test_size = math.ceil(total_data_size*(1-params.train_test_split))
    print("STATS:\n\ttraining size is "+str(train_size))
    print("\ttraining size is "+str(test_size))
    print("\n")
    
    # remove points that has no corresponding label
    feature_dict_list = [td for td in feature_dict_list if td[0] in lbl_dict]

    # shuffle the data: features and corresponding labels
    random.shuffle(feature_dict_list)
    labels = [lbl_dict[td[0]] for td in feature_dict_list if td[0] in lbl_dict]
    features = [td[1] for td in feature_dict_list]
    
    # split the data into train and test
    train_data = features[:train_size]
    test_data = features[train_size:]
    train_lbl = labels[:train_size]
    test_lbl  = labels[train_size:]

    # Train the model using the training set
    print('Model used: '+params.regr_model+"\n")
    if params.regr_model=='lr':
        regr = linear_model.LinearRegression(normalize=params.lr_normalize)
    elif params.regr_model=='rdg':
        regr = linear_model.RidgeCV(alphas=[0.1, 1.0, 10.0])
    elif params.regr_model=='lso':
        regr = linear_model.LassoCV(cv=20)
    elif params.regr_model=='ll':
        regr = linear_model.LassoLars(alpha=.1)
    elif params.regr_model=='enet':
        regr = linear_model.ElasticNetCV(cv=5, random_state=0)
    else:
        print('invalid parameter value: regr_model='+params.regr_model)
    regr.fit(train_data, train_lbl)
    
    # Print the coefficients
    print('Coefficients: ')
    for n, c in zip(feature_names, regr.coef_):
        print(str(n)+": "+str(c))
    print()
    
    # Print the training error
    if train_size>0:
        print("TRAINING ERROR")
        display_accuracy(regr, train_data, train_lbl)
    
    # Print the testing error
    if test_size>0:
        print()
        print("TESTING ERROR")
        display_accuracy(regr, test_data, test_lbl)
    
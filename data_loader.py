#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Omar Barakat
@description: This class has the methods to deal with the data file and extract features
"""
from datetime import datetime
import json
import csv
import os

class DataLoader():
    "Constructor requires a parameters object of the class Parameters"
    def __init__(self, parameters):
        self.params = parameters
        self.data_dir = os.path.join(os.path.dirname(__file__), 'Datasets/')
        
        if self.params.use_top_N:
            # find the most frequent place types
            counts = self.get_places_counts()
            N = min(self.params.top_N_places , len(counts))
            self.params.place_types = [x[0] for x in sorted(counts.items(), key=lambda i: -i[1])][:N]

    "a helper function to return the pull path of a file"
    def get_path(self, file_name):
        return os.path.join(self.data_dir, file_name)
    
    """
    a generator to iterate over the csv file
    yields array of elements
    """
    def csv_iterator(self):
        csvfile = open(self.get_path(self.params.sales_file), 'r')
        raw_data = csv.reader(csvfile, delimiter=',')
        for x in raw_data:
            yield x
        csvfile.close()
    
    """
    an iterator for the sales_granular file
    yiels rows as dictionaries of the form {datetime: count}
    """
    def csv_dict_iter(self):
        iterator = self.csv_iterator()
        header = iterator.__next__()
        header_dates = [datetime.strptime(i, "%m/%d/%y %H:%M") for i in header[1:]]
        for row in iterator:
            d = {k:v for k, v in zip(header_dates, row[1:])}
            d[header[0]] = row[0]
            yield d

    """
    this function computes the average sales for every day_of_week over the specified range
    returns the average sales for the store on every day in the format (store_code, int[7] sales)
    """
    def get_target_vars(self, date_data, 
                        first_considered_date=datetime(2017, 3, 1, 0, 0),
                        last_considered_date=datetime(2017, 6, 1, 0, 0)):
        days_cnt = [0]*7
        days_sellings = [0]*7
        for tag, val in date_data.items():
            if tag=='store_code':
                continue
            if tag < first_considered_date or tag > last_considered_date:
                continue
            day_num = tag.weekday()
            days_cnt[day_num]+=1
            days_sellings[day_num] += (0 if val== '' else int(val))
        vec = [s/(1 if c==0 else c) for s, c in zip(days_sellings, days_cnt)]
        return (date_data['store_code'], vec)
    
    """
    This function forms the statistics for all place type (example: 'store')
    Returns a dictionary of the following items:
        - max_score: highest rating
        - avg_score: average rating
        - nr_reviews: total number of reviews
        - nr_places: number of places of that type
        - hours_open: the ratio of places of that type open on every day of the week (7 features)
    """
    def get_place_type_stats(self, places_arr):
        rating_max = 0
        rating_sum = 0
        rating_cnt = 0
        review_cnt_sum = 0
        days_open = {'Monday': 0, 'Tuesday': 0, 'Wednesday': 0, 'Thursday': 0, 'Friday': 0, 'Saturday': 0, 'Sunday': 0}
        days_info_cnt = 0
        for p in places_arr:
            if 'rating' in p:
                rating_max = max(rating_max, p['rating'])
                rating_sum += p['rating']
                rating_cnt += 1
            review_cnt_sum += (0 if 'reviews' not in p else len(p['reviews']))
            
            if 'opening_hours' in p and 'weekday_text' in p['opening_hours']:
                days_info_cnt += 1
                for day_info in p['opening_hours']['weekday_text']:
                    day_name = day_info.split(':')[0]
                    if 'Closed' not in day_info:
                        days_open[day_name] += 1
        
        return {
                 'max_score': rating_max,
                 'avg_score': rating_sum/(1 if rating_cnt==0 else rating_cnt),
                 'nr_reviews': review_cnt_sum,
                 'nr_places': len(places_arr),
                 'hours_open': [ days_open[k]/(1 if days_info_cnt==0 else days_info_cnt) 
                                 for k in sorted( days_open.keys() )]
        }
    
    """
    This function takes a dictionary and optional attribute array
    Returns 1) the values of the dictionary flattened 2) array of the feature names
    """
    def flat_values(self, dictionary, attrs=[]):
        vec = []
        f_names = []
        if len(attrs)==0:
            attrs = dictionary.keys()
        for attr in attrs:
            val = dictionary[attr]
            if type(val) is list:
                vec.extend(val)
            else:
                vec.append(val)
            f_names.append(attr)
        return vec, f_names
    
    
    def extract_surrounding_features(self, json_dict, combine_surr_stats=False, features_to_use=[]):
        surr_stats = {  
            k: self.get_place_type_stats(v) 
            for k, v in json_dict['surroundings'].items()
            if len(self.params.place_types)==0 or k in self.params.place_types
        }
        feature_vec = []
        f_names = []
        for counter, k in enumerate(sorted( surr_stats.keys() )):   # loop over places stats
            vec, names = self.flat_values(surr_stats[k], features_to_use)
            if combine_surr_stats:
                feature_vec = vec if counter==0 else [f1+f2 for f1, f2 in zip(feature_vec, vec)]
                f_names = names
            else:
                feature_vec.extend(vec)
                f_names.extend([str(k)+"_"+n for n in names])
        return json_dict['store_code'], feature_vec, f_names
        
    def get_places_counts(self):
        counts = {}
        for row in self.json_iterator():
            for place in row['surroundings'].keys():
                if place in counts:
                    counts[place] += len(row['surroundings'][place])
                else:
                    counts[place] = len(row['surroundings'][place])
        return counts
    
    def get_country_counts(self):
        counts = {}
        for row in self.json_iterator():
            for p_type in row['surroundings'].keys():
                for place in row['surroundings'][p_type]:
                    add_comp = place['address_components']
                    c_name = [i['long_name'] for i in add_comp if 'long_name' in add_comp]
                    if len(c_name)==1:  c_name = c_name [0]
                    else:               continue
                    if c_name in counts:
                        counts[c_name] += 1
                    else:
                        counts[c_name] = 1
        return counts
    
    def display_place_counts(self):
        counts = self.get_places_counts(self.params.surr_file)
        print("order of places by importance")
        for place_name, cnt in sorted(counts.items(), key=lambda i: -i[1]):
            print(str(place_name)+": "+str(cnt))
    
    def json_iterator(self):
        with open(self.get_path(self.params.surr_file), 'r') as data_file:
            json_data = data_file.read()
        data = json.loads(json_data)
        for d in data:
            yield d

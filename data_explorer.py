#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Omar Barakat
This This file includes the main function for data visulaization. It allows for the following:
    1- visualizing the seasonality over day_of_the_week, day, month, year, and week_of_year
    2- visualizing the histogram of sales over different POS's
"""
from data_loader import DataLoader
from parameters import Parameters
from statistics import mean, variance
import matplotlib.pyplot as plt
from datetime import datetime

# very slow
def get_mean_var_grouped(data, grouping_fun):
    groups = {}
    for row in data:
        for d, c in row.items():
            v = 0 if c=='' else int(c)
            key = grouping_fun(d)
            if key in groups:
                groups[key].append(v)
            else:
                groups[key] = [v]
    return {k: (mean(groups[key]), variance(groups[key])) for k in groups.keys()}

def get_mean_grouped(data, grouping_fun):
    groups = {}
    groups_cnt = {}
    for row in data:
        for d, c in row.items():
            v = 0 if c=='' else int(c)
            key = grouping_fun(d)
            if key in groups:
                groups[key] += v
                groups_cnt[key] += 1
            else:
                groups[key] = v
                groups_cnt[key] = 1
    return {k: groups[k]/groups_cnt[k] for k in groups.keys()}

def plot_dict(d):
    points = [c for _, c in sorted(d.items(), key=lambda i: -i[0])]
    plt.figure()
    plt.plot(points)

def plot_seasonality(data):
    def group_by_day_of_week(date):
        return date.weekday()
    def group_by_year(date):
        return date.year
    def group_by_month(date):
        return date.month
    def group_by_week_of_year(date):
        return date.isocalendar()[1]
    def group_by_day_of_year(date):
        return date.timetuple().tm_yday

    grouping_funs = [group_by_day_of_week, 
                     group_by_year, 
                     group_by_month, 
                     group_by_week_of_year, 
                     group_by_day_of_year]
    
    for fn in grouping_funs:
        stats = get_mean_grouped(data, fn)
        print("for fn "+str(fn).split(' ')[1]+", groups are")
        print(stats.keys())
        plot_dict(stats)


def get_target_vars(date_data, 
                        first_considered_date=datetime(2017, 3, 1, 0, 0),
                        last_considered_date=datetime(2017, 6, 1, 0, 0)):
        days_cnt = [0]*7
        days_sellings = [0]*7
        for tag, val in date_data.items():
            if tag=='store_code':
                continue
            if tag < first_considered_date or tag > last_considered_date:
                continue
            day_num = tag.weekday()
            days_cnt[day_num]+=1
            days_sellings[day_num] += (0 if val== '' else int(val))
        return [s/(1 if c==0 else c) for s, c in zip(days_sellings, days_cnt)]
        
def plot_hist(data, loader):
    vals = [sum(get_target_vars(row)) for row in data]
    plt.hist(vals, bins=100)


    
    
    
    
if __name__ == "__main__":
    params = Parameters()
    loader = DataLoader(params)
        
    latest_date = datetime(2017, 6, 25, 22, 0)
#    earliest_date = latest_date-datetime.timedelta(days = time_period)
    earliest_date = datetime(2017, 1, 1, 0, 0)
    
    data = []
    generator = loader.csv_dict_iter()
    for row in generator:
        store_code = row.pop('store_code', None)
        data.append({k: row[k] for k in row.keys() if k>=earliest_date and k<=latest_date})

    plot_seasonality(data)
    plot_hist(data, loader)
    
    

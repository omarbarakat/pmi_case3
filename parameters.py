#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: omar

@description: this class is a wrapper for running parameters.
"""

class Parameters():
    def __init__(self):
        self.sales_file = 'sales_granular.csv'
        self.surr_file = 'Surroundings.json'
        self.train_test_split = 1.0     # ratio of the training data to all data available
        
        # specify used features from the following features. Leave empty for all.
        # 'max_score', 'avg_score', 'nr_reviews', 'nr_places', 'hours_open'
        self.features_to_use=['nr_places']

        # wheather to aggregate all places types or get stats for every place type individually
        self.combine_surr_stats = False
        
        # the following flags determine wheather to use top N places or a specific list of place types
        self.use_top_N = True
        self.use_places_list = False and not self.use_top_N
        
        # the top N places to consider. 100 will use all
        self.top_N_places = 50
        
        # list of place types you want to consider. Leave empty for all. 
        # For insights check place_type_stats.csv
        self.place_types = [
#                            'store', 
#                            'restaurant', 
#                            'doctor', 
#                            'clothing_store', 
#                            'real_estate_agency', 
#                            'hair_care',
#                            'dentist',
#                            'beauty_salon',
#                            'lawyer',
#                            'bar',
#                            'home_goods_store',
#                            'transit_station',
#                            'bank',
#                            'travel_agency',
#                            'cafe',
#                            ''
                            ] if self.use_places_list else []
        
        # regr model can take values 
        # 'lr': Linear Regression, 'rdg': Ridge, 'lso': Lasso, 'enet': Elastic Net, 'll': LassoLars
        self.regr_model = 'lso'
        
        self.lr_normalize = True
# PMI Case 3: Estimate POS Activity

This is  business case to find the surroundings and respective amenities that lead to top POS performance.

The code has two entry points:
	- driver.py: manages the pipeline for training and testing
	- data_explorer.py: shows different plots for the data

Please note:
For the code to work the dataset must be copied to the directory ./Datasets. The dataset should include the files Surroundings.json and sales_granular.csv
